import socket
import json
import bencode
from threading import Thread
import sys

class Server():
    con_status = 0
    peers = []
    peers_forserv={}
    reg_port = sys.argv[6]
    reg_ip = sys.argv[4]
    def __init__(self):
        conn_th = Thread(target=self.peer_connect())
        conn_th.start()

    def peer_connect(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind((self.reg_ip, int(self.reg_port)))
        while True:
            data, adr = sock.recvfrom(1024)
            data = bencode.decode(data.decode())
            data = json.dumps(data)
            json_data = json.loads(data)
            self.pak_type(json_data)

    def add_peer(self, json_data):
        username = json_data['username']
        ipv4 = json_data['ipv4']
        port = json_data['port']
        peer_data = {"username": username, "ipv4": ipv4, "port": port}
        tid = json_data['txid']
        if len(self.peers) == 0:
            ushort = 0
            list_peer_data = {str(ushort): peer_data}
            self.peers_forserv[int(tid)] = [ipv4, port]
            self.peers.append(list_peer_data)
        else:
            try:
                self.peers_forserv[int(tid)]
            except KeyError:
                self.peers.append({str(len(self.peers)):peer_data})
                self.peers_forserv[int(tid)] = [ipv4, port]

# {"type":"list", "txid":<ushort>, "peers": {<PEER_RECORD*>}}
    def pak_type(self, data):
        if data['type'] == 'hello' and data['port'] != 0 and data['ipv4'] != "0.0.0.0":
            self.add_peer(data)
        elif data['type'] == 'getlist':
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            ip = self.peers_forserv[int(data['txid'])][0]
            port = self.peers_forserv[int(data['txid'])][1]
            sock.connect((ip, int(port)))
            list_peer = {"type":"list", "txid":"123", "peers":self.peers}
            list_peer = json.dumps(list_peer)
            json_data = json.loads(list_peer)
            data = bencode.encode(json_data)
            sock.send(data.encode())
        else:
            self.peers_forserv={}
            self.peers=[]

s = Server()
#conn.close()