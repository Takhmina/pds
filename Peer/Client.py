import socket
import time
from threading import Thread
import bencode
import sys
import json
from tkinter import *
from bencode import BTL

class Client:
    txid = sys.argv[2]
    username = str(sys.argv[4])
    chat_ip = sys.argv[6]
    chat_port = sys.argv[8]
    reg_ip = sys.argv[10]
    reg_port = sys.argv[12]
    con_state = 0
    peer_list = []
    last_ack = 0
    peer_names = []
    mes = []
    port_peer = {}
    log = "You are not connected"
    def con_serv(self):
        self.con_state = 1
        self.log = ""
        sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        sock.connect((self.reg_ip, int(self.reg_port)))
        while True:
            json_data = {"type":"hello", "txid": self.txid, "username": self.username, "ipv4": self.chat_ip, "port": self.chat_port}
            data = bencode.encode(json_data)
            sock.send(data.encode())
            time.sleep(10)
            if self.con_state is 0:
                return
        sock.close()

    def recv_mesage(self):
        if self.con_state == 1:
            self.log = ""
            sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
            sock.bind((self.chat_ip, int(self.chat_port)))
            while True:
                data, addr = sock.recvfrom(1024)
                data = bencode.decode(data.decode())
                data = json.dumps(data)
                json_data = json.loads(data)
                if str(json_data['type']) == "list":
                    buf_list = json_data['peers']
                    i = 0
                    for val in buf_list:
                        self.peer_list.append(val[str(i)])
                        i = i + 1

                if str(json_data['type']) == "message":
                    print("Message from " + str(json_data['from']) + ":")
                    print(str(json_data['message']))
                    self.mes.append("Message from " + str(json_data['from']) + ":" + "\n" + str(json_data['message']))
                    sock_1 = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    sock_1.connect((str(addr[0]), self.port_peer[str(json_data['from'])]))
                    json_data = {"type":"ack", "txid":self.txid}
                    data = bencode.encode(json_data)
                    sock_1.send(data.encode())
                    sock_1.close()
                elif str(json_data['type']) == "ack":
                    self.last_ack = 1
        else:
            self.log = "You are not connected"
            return

    def close_con(self):
        time.sleep(1)
        sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        sock.connect((self.reg_ip, int(self.reg_port)))
        json_data = {"type":"hello", "txid": self.txid, "username": self.username, "ipv4": "0.0.0.0", "port": 0}
        data = bencode.encode(json_data)
        sock.send(data.encode())
        sock.close()
        self.con_state = 0
        self.log = "You are not connected"

    def send_message(self, to_m, message):
        if self.con_state == 1:
            self.log = ""
            to_ip = ''
            to_port = 0
            if len(self.peer_list) != 0:
                for val in self.peer_list:
                    try:
                        if str(val['username']) == to_m:
                            to_ip = val['ipv4']
                            to_port = int(val['port'])
                    except KeyError:
                        self.get_list()
                        time.sleep(0.1)
            else:
                self.get_list()
                time.sleep(0.1)
                for val in self.peer_list:
                    if str(val['username']) == to_m:
                        to_ip = val['ipv4']
                        to_port = int(val['port'])

            if to_ip == '':
                print("There id no peer as " + to_m)
            else:
                sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                sock.connect((to_ip, to_port))
                json_data = {"type": "message", "txid": self.txid, "from": str(self.username), "to": str(to_m),"message": str(message)}
                data = bencode.encode(json_data)
                sock.send(data.encode())
                sock.close()
                ack_th = Thread(target=self.mes_ack, args=(to_m, message))
                ack_th.setDaemon(True)
                ack_th.start()
        else:
            self.log = "You are not connected"
            return

    def mes_ack(self, to_m, mes):
        time.sleep(5)
        if self.last_ack == 0:
            print("Try again '" + mes )
        else:
            self.last_ack = 0
    def get_list(self):
        if self.con_state == 1:
            self.log = ""
            sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
            sock.connect((self.reg_ip, int(self.reg_port)))
            json_data = {"type":"getlist", "txid":self.txid}
            data = bencode.encode(json_data)
            sock.send(data.encode())
        else:
            self.log = "You are not connected"

    def peer_name(self):
        if self.con_state == 1:
            self.log = ""
            time.sleep(0.1)
            self.get_list()
            time.sleep(0.1)
            for val in self.peer_list:
                self.peer_names.append(str(val['username']))
                self.port_peer[str(val['username'])] = int(val['port'])
            for v in self.peer_names:
                if str(v) != self.username:
                    print(str(v) + ", ")
        else:
            self.log = "You are not connected"

    def my_info(self):
        return sys.argv

    def con_serv_th(self):
        con = Thread(target=self.con_serv)
        con.setDaemon(True)
        con.start()
        rec = Thread(target=self.recv_mesage)
        rec.setDaemon(True)
        rec.start()

    def update_list(self):
        if self.con_state == 1:
            self.log = ""
            self.peer_list = []
            self.peer_names = []
            self.peer_name()
        else:
            self.log = "You are not connected"

def main():
    c = Client()

root = Tk()
c = Client()
canvas = Canvas(root, width=800, height=500, bd=0, highlightthickness=0)
canvas.create_text(10, 10, text="Username: " + c.my_info()[2], anchor=W)
canvas.create_text(10, 30, text="IP: " + c.my_info()[3], anchor=W)
canvas.create_text(10, 50, text="Port: " + c.my_info()[4] , anchor=W)

button1 = Button(root, text = "Connect to server", command = c.con_serv_th, anchor = W)
button1.configure(width = 15, activebackground = "#33B5E5", relief = FLAT, background="#33B5E5")
button1_window = canvas.create_window(10, 80, anchor=W, window=button1)

button2 = Button(root, text = "Close connection", command = c.close_con, anchor = W)
button2.configure(width = 15, activebackground = "#33B5E5", relief = FLAT, background="#33B5E5")
button2_window = canvas.create_window(140, 80, anchor=W, window=button2)

peer = canvas.create_text(10, 110, text="Avaliable peers: " + str(c.peer_names), anchor=W)
def update_peer():
    c.update_list()
    text = ""
    for val in c.peer_names:
        if len(c.peer_names) > 1:
            text = text  + str(val)+ " "
        else:
            text = str(val)
    canvas.itemconfigure(peer, text="Avaliable peers: " + text)

button2 = Button(root, text = "Update", command = update_peer, anchor = W)
button2.configure(width = 15, activebackground = "#33B5E5", relief = FLAT, background="#33B5E5")
button2_window = canvas.create_window(10, 140, anchor=W, window=button2)

to_tex = canvas.create_text(10, 160, text="To: ", anchor=W)
e1 = Entry(canvas)
e1.configure(width=15)
e1_w = canvas.create_window(10, 180, anchor=W, window=e1)

Mesage_tex = canvas.create_text(10, 200, text="Message: ", anchor=W)
e2 = Entry(canvas)
e2.configure(width=100)
e2_w = canvas.create_window(10, 220, anchor=W, window=e2)

log_tex = canvas.create_text(10, 460, text="Log: ", anchor=W)

def send_mes():
    usname = e1.get()
    mes = e2.get()
    c.send_message(str(usname), str(mes))
    text = "You wtite to: " + str(usname) + "\n" + str(mes)
    c.mes.append(text)


button3 = Button(root, text = "Send", command =send_mes, anchor = W)
button3.configure(width = 15, activebackground = "#33B5E5", relief = FLAT, background="#33B5E5")
button3_window = canvas.create_window(10, 240, anchor=NW, window=button3)

tex = canvas.create_text(10, 280, width=300, anchor=NW)

def update_ip():
    text = ""
    try:
        if len (c.mes) < 6:
            for val in c.mes:
                text = text + str(val) + "\n"
        else:
            c.mes.pop(0)
            for val in c.mes:
                text = text + str(val) + "\n"
        canvas.itemconfigure(tex, text=text)
        canvas.itemconfigure(log_tex, text="Log: " + c.log)
        root.after(1000, update_ip)
    except StopIteration:
        pass
update_ip()

canvas.pack()
root.mainloop()
c.close_con()


# Info about user
#canvas = Canvas(root, width=800, height=500, bd=0, highlightthickness=0)
#canvas.create_text(10, 10, text="Username: " + c.my_info()[2], anchor=W)
#canvas.create_text(10, 30, text="IP: " + c.my_info()[3], anchor=W)
#canvas.create_text(10, 50, text="Port: " + c.my_info()[4], anchor=W)
#canvas.pack()


